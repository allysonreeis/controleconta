package model;

import java.util.ArrayList;

public class Conta {
	private Double saldo;
	private ArrayList<String> transacao;
	private String numeroConta;

	public Conta (String numeroConta) {
		this.numeroConta = numeroConta;
	}
	
	public void setSaldo (Double saldo) {
		this.saldo = saldo;
	}
	public Double getSaldo () {
		return this.saldo;
	}

	public void setTransacao (ArrayList<String> transacao) {
		this.transacao = transacao;
	}
	public ArrayList<String> getTransacao () {
		return transacao;
	}

	public void setNumeroConta (String numeroConta) {
		this.numeroConta = numeroConta;
	}
	public String getNumeroConta () {
		return numeroConta;
	}
}
