package model;

public class ContaPoupanca extends Conta{
	private String cartaoDebito;

	public ContaPoupanca(String numeroConta) {
		super(numeroConta);
	}
	
	public String getCartaoDebito() {
		return cartaoDebito;
	}

	public void setCartaoDebito(String cartaoDebito) {
		this.cartaoDebito = cartaoDebito;
	}
	
	
}
