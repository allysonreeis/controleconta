package controller;

import java.util.ArrayList;

import model.PessoaJuridica;

public class ControlePessoaJuridica {
	
	private ArrayList<PessoaJuridica> listaPessoaJuridica;
    
    public ControlePessoaJuridica () {
        listaPessoaJuridica = new ArrayList<PessoaJuridica>();
    }

    public ArrayList<PessoaJuridica> getListaPessoaJuridica() {
        return listaPessoaJuridica;
    }

    public void setListaPessoaJuridica(ArrayList<PessoaJuridica> listaPessoaJuridica) {
        this.listaPessoaJuridica = listaPessoaJuridica;
    }
    
    
    
    public void adicionarPessoaJuridica (PessoaJuridica umaPessoa) {
        listaPessoaJuridica.add(umaPessoa);
    }
    
    public void removerPessoaJuridica (PessoaJuridica umaPessoa) {
        listaPessoaJuridica.remove(umaPessoa);
    }
    
    public PessoaJuridica pesquisarPessoaJuridica (String umNome) {
        for (PessoaJuridica umaPessoa : listaPessoaJuridica) {
            if (umaPessoa.getNome().equalsIgnoreCase(umNome)) return umaPessoa;
        }
        return null;
    }
}
