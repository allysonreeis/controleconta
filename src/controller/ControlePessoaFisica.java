package controller;

import java.util.ArrayList;

import model.PessoaFisica;

public class ControlePessoaFisica {
	private ArrayList<PessoaFisica> listaPessoaFisica;
    
	public ControlePessoaFisica () {
		listaPessoaFisica = new ArrayList<PessoaFisica>();
	}
	    
	public void adicionaPessoaFisica (PessoaFisica umaPessoa) {
		listaPessoaFisica.add(umaPessoa);
	}
	    
	public void removePessoaFisica (PessoaFisica umaPessoa) {
		listaPessoaFisica.remove(umaPessoa);
	}
	        
	public ArrayList<PessoaFisica> getListaPessoaFisica() {
		return listaPessoaFisica;
	}

	public void setListaPessoaFisica(ArrayList<PessoaFisica> listaPessoaFisica) {
		this.listaPessoaFisica = listaPessoaFisica;
	}
	
	public PessoaFisica pesquisaPessoaFisica (String umNome) {
		for (PessoaFisica umaPessoa : listaPessoaFisica) {
			if (umaPessoa.getNome().equalsIgnoreCase(umNome)) return umaPessoa;
	    }
	        return null;
	}
	
}
